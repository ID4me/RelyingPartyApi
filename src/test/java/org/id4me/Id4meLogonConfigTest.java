/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */
package org.id4me;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.id4me.config.Id4meClaimsParameters;
import org.id4me.config.Id4meClaimsParameters.Entry;
import org.id4me.config.Id4meProperties;
import org.jitsi.dnssec.SRRset;
import org.jitsi.dnssec.validator.TrustAnchorStore;
import org.jitsi.dnssec.validator.ValidatingResolver;
import org.junit.Before;
import org.junit.Test;
import org.xbill.DNS.Name;
import org.xbill.DNS.SimpleResolver;

/**
 *
 * @author Sven Woltmann
 */
public class Id4meLogonConfigTest {

	private static final String CLIENT_NAME = "My-ID4me-Client";
	private static final String REDIRECT_URI = "https://my-domain.org/id4me/logon";
	private static final String LOGO_URI = "https://my-domain.org/my-logo.png";
	private static final String DNSSEC_ROOT_KEY = ". IN DS 19036 8 2 0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
	private static final boolean DNSSEC_REQUIRED = false;
	private static final String DNS_RESOLVER = "8.8.8.8";
	private static final Path REGISTRATION_DATA_PATH = Paths.get("/opt/registrationdata/");

	@Before
	public void reset() throws ReflectiveOperationException {
		// Field field = Id4meLogon.class.getDeclaredField("REGISTRATION_DATA_PATHS");
		// field.setAccessible(true);
		//
		// Set<String> paths = (Set<String>) field.get(null);
		//
		// paths.clear();
	}

	@Test
	public void testConfigInjectionWithDNSSECRequired() throws Exception {
		Id4meProperties properties = buildProperties();
		properties.setDnssecRequired(true);

		Id4meLogon logonHandler = new Id4meLogon(properties, buildClaimsParams());

		validateLogonHandler(logonHandler, properties);
	}

	@Test
	public void testConfigInjectionWithDNSSECNotRequired() throws Exception {
		Id4meProperties properties = buildProperties();
		properties.setDnssecRequired(false);

		Id4meLogon logonHandler = new Id4meLogon(properties, buildClaimsParams());

		validateLogonHandler(logonHandler, properties);
	}

	@Test
	@SuppressWarnings("unused")
	public void testTwoConfigurationsWithDifferentPathsAllowed() throws Exception {
		Id4meProperties propertiesSite1 = buildProperties();
		propertiesSite1.setRegistrationDataPath("/opt/registrationdata/site1");
		Id4meLogon logonSite1 = new Id4meLogon(propertiesSite1, buildClaimsParams());

		Id4meProperties propertiesSite2 = buildProperties();
		propertiesSite2.setRegistrationDataPath("/opt/registrationdata/site2");
		Id4meLogon logonSite2 = new Id4meLogon(propertiesSite2, buildClaimsParams());
	}

	private Id4meProperties buildProperties() {
		return new Id4meProperties() //
				.setClientName(CLIENT_NAME) //
				.setRedirectURI(REDIRECT_URI) //
				.setLogoURI(LOGO_URI) //
				.setDnssecRootKey(DNSSEC_ROOT_KEY) //
				.setDnssecRequired(DNSSEC_REQUIRED) //
				.setDnsResolver(DNS_RESOLVER) //
				.setRegistrationDataPath(REGISTRATION_DATA_PATH.toString());
	}

	static Id4meClaimsParameters buildClaimsParams() {
		return new Id4meClaimsParameters()
				.addEntry(new Entry().setName("email").setEssential(true).setReason("Needed to create the profile"))
				.addEntry(new Entry().setName("name").setReason("Displayname in the user data"))
				.addEntry(new Entry().setName("given_name"));
	}

	@Test
	public void testConfigFileAndDNSSECNotSpecified() throws Exception {
		Id4meLogon logonHandler = new Id4meLogon(resourceAsFile("/id4me.properties"),
				resourceAsFile("/claims.parameters.json"));

		validateLogonHandler(logonHandler, buildProperties().setDnssecRequired(true));
	}

	@Test
	public void testConfigFileAndDNSSECRequired() throws Exception {
		Id4meLogon logonHandler = new Id4meLogon(resourceAsFile("/id4me.properties.dnssec_true"),
				resourceAsFile("/claims.parameters.json"));

		validateLogonHandler(logonHandler, buildProperties().setDnssecRequired(true));
	}

	@Test
	public void testConfigFileAndDNSSECNotRequired() throws Exception {
		Id4meLogon logonHandler = new Id4meLogon(resourceAsFile("/id4me.properties.dnssec_false"),
				resourceAsFile("/claims.parameters.json"));

		validateLogonHandler(logonHandler, buildProperties().setDnssecRequired(false));
	}

	@Test
	public void testConfigFileWithDNSSECRootKeyTypo() throws Exception {
		// Tests that the old parameter name "dnsssec_root_key" (three 's') still works
		// for backwards compatibility
		Id4meLogon logonHandler = new Id4meLogon(resourceAsFile("/id4me2.properties.dnssec_root_key_typo"),
				resourceAsFile("/claims.parameters.json"));

		validateLogonHandler(logonHandler, buildProperties().setDnssecRequired(true));
	}

	void validateLogonHandler(Id4meLogon logonHandler, Id4meProperties properties)
			throws ReflectiveOperationException, NoSuchFieldException, IllegalAccessException {

		// -- id4meProperties --

		assertThatFieldIs(logonHandler, "clientName", properties.getClientName());
		assertThatFieldIs(logonHandler, "redirectUri", properties.getRedirectURI());
		assertThatFieldIs(logonHandler, "logoUri", properties.getLogoURI());

		Id4meResolver resolver = (Id4meResolver) readPrivateField(logonHandler, "resolver");
		ValidatingResolver validatingResolver = (ValidatingResolver) readPrivateField(resolver, "vr");

		String actualRootKey = extractRootKey(validatingResolver);
		assertThat(actualRootKey, is(properties.getDnssecRootKey()));

		assertThatFieldIs(resolver, "dnssecRequired", Boolean.valueOf(properties.isDnssecRequired()));

		SimpleResolver simpleResolver = (SimpleResolver) readPrivateField(validatingResolver, "headResolver");
		assertThat(simpleResolver.getAddress().getHostString(), is(properties.getDnsResolver()));

		assertThatFieldIs(logonHandler, "registrationDataPath", Paths.get(properties.getRegistrationDataPath()));

		// -- claimsParameters --

		Field claimsConfigField = logonHandler.getClass().getDeclaredField("claimsConfig");
		claimsConfigField.setAccessible(true);
		Id4meClaimsConfig claimsConfig = (Id4meClaimsConfig) claimsConfigField.get(logonHandler);

		assertThat(claimsConfig.getClaimsParam(), is("{\"userinfo\":{" //
				+ "\"name\":{\"reason\":\"Displayname in the user data\"}," //
				+ "\"given_name\":null,"
				+ "\"email\":{\"reason\":\"Needed to create the profile\",\"essential\":true}}}"));
	}

	private void assertThatFieldIs(Object holder, String fieldName, Object expectedValue)
			throws ReflectiveOperationException {
		Object value = readPrivateField(holder, fieldName);
		assertThat(value, is(expectedValue));
	}

	private Object readPrivateField(Object object, String fieldName)
			throws NoSuchFieldException, IllegalAccessException {
		Field field = object.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		return field.get(object);
	}

	private String resourceAsFile(String name) throws URISyntaxException {
		URL resource = this.getClass().getResource(name);
		Path path = Paths.get(resource.toURI());
		return path.toString();
	}

	private String extractRootKey(ValidatingResolver validatingResolver) {
		TrustAnchorStore trustAnchors = validatingResolver.getTrustAnchors();
		SRRset srrSet = trustAnchors.find(Name.root, 1);
		return ". IN DS " + srrSet.first().rdataToString();
	}

}
