/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.net.IDN;

import org.junit.Test;

/**
 *
 * @author Peter Hoebel
 */
public class Id4meDomainValidaterTest {

	@Test
	public void testDomainValidataor() {

		String[] validDdomains = { "de", "com", "möbel.de", "peter.höbel@test.de", "peter.höbel.test.de", "_domainid.de",
				"peter.hoebel.phoebel.test.de" };
		String[] invalidDdomains = { "_de", "1&1.de", "test._de" };

		for (String id4me : validDdomains) {
			id4me = IDN.toASCII(id4me);
			assertThat(id4me, Id4meValidator.isValidUserid(id4me), is(true));
		}

		for (String id4me : invalidDdomains) {
			id4me = IDN.toASCII(id4me);
			assertThat(id4me, Id4meValidator.isValidUserid(id4me), is(false));
		}

	}

}
