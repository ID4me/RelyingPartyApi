/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */
package org.id4me;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONObject;
import org.junit.Test;

/**
 *
 * @author Sven Woltmann
 */
public class Id4meIdentityAuthorityStorageTest {

	@Test
	public void testLoadIauDataFromFile() throws Exception {
		Path registrationDataPath = Paths.get("/registration-data-path");
		Id4meIdentityAuthorityStorage2 storage = Id4meIdentityAuthorityStorage2.INSTANCE;

		boolean success = storage.loadIauDataFromFile(registrationDataPath, "test-iau");
		assertThat(success, is(true));

		Id4meIdentityAuthorityData data = storage.getIauData(registrationDataPath, "test-iau");
		assertThat(data.getIau(), is("test-iau"));
		assertThat(data.getClientId(), is("some_client_id"));
		assertThat(data.getClientSecret(), is("some_client_secret"));
	}

	@Test
	public void testSaveRegistrationData() throws FileNotFoundException, IOException {
		Path registrationDataPath = Paths.get("/registration-data-path");
		String tempDir = System.getProperty("java.io.tmpdir");
		Id4meIdentityAuthorityStorage2 storage = Id4meIdentityAuthorityStorage2.INSTANCE;

		JSONObject registrationData = new JSONObject();
		registrationData.put("client_id", "some_client_id");
		registrationData.put("client_secret", "some_client_secret");

		storage.saveRegistrationData(registrationDataPath, "test-iau-save", registrationData);

		File file = new File(new File(tempDir), "test-iau-save.json");
		assertThat(file.exists(), is(true));

		// Clean up
		file.delete();
	}

}
