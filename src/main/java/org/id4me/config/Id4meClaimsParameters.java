/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Sven Woltmann sven.woltmann@androidpit.de
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me.config;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sven Woltmann
 */
public class Id4meClaimsParameters {

	private List<Entry> entries = new ArrayList<>();

	public Id4meClaimsParameters addEntry(Entry entry) {
		entries.add(entry);
		return this;
	}

	public List<Entry> getEntries() {
		return entries;
	}

	public static class Entry {

		private String name;
		private boolean essential;
		private String reason;

		public String getName() {
			return name;
		}

		public Entry setName(String name) {
			this.name = name;
			return this;
		}

		public boolean isEssential() {
			return essential;
		}

		public Entry setEssential(boolean essential) {
			this.essential = essential;
			return this;
		}

		public String getReason() {
			return reason;
		}

		public Entry setReason(String reason) {
			this.reason = reason;
			return this;
		}

	}

}
