
/**
 * Provides the classes necessary to logon an <b>Id4ME</b> user id at an
 * <b>Id4ME</b> identity authority and get and validate an userinfo object for
 * this user.
 * <p>
 * Example logon flow:
 * <ul>
 * <li>Create an {@link org.id4me.Id4meSessionData} object by calling
 * {@link org.id4me.Id4meLogon#createSessionData(String, boolean)}</li>
 * <li>Rerdorect the client to the authorization uri received by
 * {@link org.id4me.Id4meLogon#authorize(org.id4me.Id4meSessionData)}</li>
 * <li>Get the parameter <code>code</code> from the http request at redirection
 * end point</li>
 * <li>Call
 * {@link org.id4me.Id4meLogon#authenticate(org.id4me.Id4meSessionData, String)}
 * to auathenticate the user at the InetId identity authority</li>
 * <li>Call {@link org.id4me.Id4meLogon#userinfo(org.id4me.Id4meSessionData)} to
 * receive the userinfo data from the identity agent and save it in the
 * Id4meSessionData</li>
 * </ul>
 * <p>
 * External libraries, used by the project: <br>
 * <br>
 * dnsjava-2.1.7.jar <br>
 * javax.servlet-api-4.0.0-b07.jar <br>
 * json-20170516.jar <br>
 * nimbus-jose-jwt-4.33.jar <br>
 * dnssecjava-1.1.3.jar <br>
 * slf4j-api-1.7.25.jar
 */

package org.id4me;
