/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me;

import org.json.JSONObject;

/**
 * @author Peter Hoebel
 *
 */
class Id4meIdentityAuthorityData {

	private String iau;
	private String clientId;
	private String clientSecret;
	private JSONObject wellKnown;
	private JSONObject registrationData;

	String getIau() {
		return iau;
	}

	void setIau(String iau) {
		this.iau = iau;
	}

	String getClientId() {
		return clientId;
	}

	void setClientId(String clientId) {
		this.clientId = clientId;
	}

	String getClientSecret() {
		return clientSecret;
	}

	void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	JSONObject getWellKnown() {
		return wellKnown;
	}

	void setWellKnown(JSONObject wellKnown) {
		this.wellKnown = wellKnown;
	}

	JSONObject getRegistrationData() {
		return registrationData;
	}

	void setRegistrationData(JSONObject registrationData) {
		this.registrationData = registrationData;
	}

}
