![id4me](http://id4me.org/wp-content/uploads/2018/03/ID4me_logo_2c_pos_rgb.png)

ID4me Java Relying Party API
----------------------------

_Version 2.13 - 27 March 2019_

This library provides the functionalities necessary to implement an **ID4me Relying Party** in **Java**, integrating it into your own website's login flow.

Please note that at this point in time the ID4me service is still experimental. No guarantee is given that the present API, or even the underlying specification and architecture, will not change before final release.


Documentation
-------------

You should first make sure you understand the architecture and basic technical elements of the ID4me platform. You can refer to the "Technical Overview" document available in the [Documents section of the ID4me website](https://id4me.org/documents).

After that, you should read the [**Programmer's Guide**](https://id4me.org/files/rp-api/java/2.1/ID4me-JavaRelyingPartyAPI-ProgrammersGuide-v2.1.pdf) from the ID4me website to learn how to use the API. You can also refer to the **Javadoc code documentation** in the _doc/_ subdirectory of the repository.


License
--------------------

The library is (C) OX Software GmbH and it is distributed as free software under the MIT license. See the _LICENSE.txt_ file in the distribution.